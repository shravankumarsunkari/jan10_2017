from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import time
import json
import csv
import re
driver=webdriver.Firefox()
#driver.implicitly_wait(10)
driver.get("http://www.optasports.com/football-team-and-player-widget-showcase.aspx")

teams=[]
count=0

#select = Select(driver.find_element_by_id("team_p0"))
select = driver.find_element_by_id('team_p0')
for team in select.find_elements_by_tag_name('option'):#works only for single team
	if "Select team" in team.text:
		continue
	teams.append(team.text)
	count=count+1

print "Total Teams found=",count,"\n\n"
print "Scraping each team...\n"
dict_football={}
with open('footBallData.csv','w') as csvfile:
	writer=csv.writer(csvfile,quoting=csv.QUOTE_NONE,lineterminator='\n')
	for team_name in teams:
		select = Select(driver.find_element_by_id("team_p0"))
		select.select_by_visible_text(team_name)
		time.sleep(1)
		print "Team Name: ",team_name
		
		players_names=[]
		players=driver.find_element_by_id('player_p0')
		for player in players.find_elements_by_tag_name('option'):
			if "Select player" in player.text:
				continue
			#print "Player :",player.text
			players_names.append(player.text)
			#player.click()
		
		player_details=[]
		#Get each player details
		print "\nscraping each player..."
		for player in players_names:
			select = Select(driver.find_element_by_id("player_p0"))
			select.select_by_visible_text(player)
			time.sleep(1)

			about_player=driver.find_element_by_id("playercompare-table-2").text
			details=about_player.split("\n")

			player_name=""
			player_position=""
			player_games_played=str(0)
			player_minutes_played=str(0)
			player_stars=str(0)
			player_substitution_on=str(0)
			player_substitution_off=str(0)

			passes=str(0)
			passing_accuracy=str(0)
			duels_won=str(0)
			duels_lost=str(0)
			duels_won_per=str(0)
			aerial_duels_won=str(0)
			aerial_duels_lost=str(0)
			aerial_duels_won_per=str(0)
			recoveries=str(0)
			
		
			goals_conceded=str(0)
			mins_per_goal=str(0)
			clean_sheets=str(0)
			saves=str(0)
			saves_per_min=str(0)
			shots_saved_per=str(0)
			catches=str(0)
			punched=str(0)

			
			player_name=details[0]
			player_position=details[2].split()[0]
			player_games_played=details[3].split()[0]
			player_games_played =re.sub('[,]','',player_games_played)
			player_minutes_played=details[4].split()[0]
			player_minutes_played =re.sub('[,]','',player_minutes_played)
			player_stars=details[5].split()[0]
			if 6<len(details):
				player_substitution_on=details[6].split()[0]
				player_substitution_on =re.sub('[,]','',player_substitution_on)
			if 7<len(details):
				player_substitution_off=details[7].split()[0]
				player_substitution_off =re.sub('[,]','',player_substitution_off)


			about_player=driver.find_element_by_id("playercompare-container-2").text
			details=about_player.split("\n")
			
			print "Current Player: ",player_name
			
			if details[0]=="General":
				passes=details[5].split()[0]
				passes =re.sub('[,]','',passes)
				if passes=='-':
					passes=str(0)

				passing_accuracy=details[6].split("%")[0]
				if passing_accuracy[0]=='-':
					passing_accuracy=str(0)

				duels_won=details[8].split()[0]
				duels_won =re.sub('[,]','',duels_won)
				if duels_won[0]=='-':
					duels_won=str(0)

				duels_lost=details[9].split()[0]
				duels_lost =re.sub('[,]','',duels_lost)
				if duels_lost[0]=='-':
					duels_lost=str(0)

				duels_won_per=details[10].split("%")[0]
				if duels_won_per[0]=='-':
					duels_won_per=str(0)

				aerial_duels_won=details[11].split()[0]
				if aerial_duels_won[0]=='-':
					aerial_duels_won=str(0)

				aerial_duels_lost=details[12].split()[0]
				if aerial_duels_lost[0]=='-':
					aerial_duels_lost=str(0)

				aerial_duels_won_per=details[13].split("%")[0]
				if aerial_duels_won_per[0]=='-':
					aerial_duels_won_per=str(0)

				recoveries=details[14].split()[0]
				recoveries=re.sub('[,]','',recoveries)
				if recoveries[0]=='-':
					recoveries=str(0)
				
			elif details[0]=="Goalkeeping":
				goals_conceded=details[4].split()[0]
				if goals_conceded[0]=='-':
					goals_conceded=str(0)

				mins_per_goal=details[5].split()[0]
				if mins_per_goal[0]=='-':
					mins_per_goal=str(0)

				clean_sheets=details[6].split()[0]
				if clean_sheets[0]=='-':
					clean_sheets=str(0)

				saves=details[7].split()[0]
				if saves[0]=='-':
					saves=str(0)

				saves_per_min=details[8].split()[0]
				if saves_per_min[0]=='-':
					saves_per_min=str(0)

				shots_saved_per=details[9].split("%")[0]
				if shots_saved_per[0]=='-':
					shots_saved_per=str(0)

				catches=details[10].split()[0]
				if catches[0]=='-':
					catches=str(0)

				punched=details[11].split()[0]
				if punched[0]=='-':
					punched=str(0)
			#REMOVE UNICODE CHARACTERS
			
			player_games_played=int(player_games_played.encode('utf-8'))
			player_minutes_played=int(player_minutes_played.encode('utf-8'))
			player_stars=int(player_stars.encode('utf-8'))
			player_substitution_on=int(player_substitution_on.encode('utf-8'))
			player_substitution_off=int(player_substitution_off.encode('utf-8'))
			passes=int(passes.encode('utf-8'))
			duels_won=int(duels_won.encode('utf-8'))
			duels_lost=int(duels_lost.encode('utf-8'))
			aerial_duels_won=int(aerial_duels_won.encode('utf-8'))
			aerial_duels_lost=int(aerial_duels_lost.encode('utf-8'))
			recoveries=int(recoveries.encode('utf-8'))
			goals_conceded=int(goals_conceded.encode('utf-8'))
			clean_sheets=int(clean_sheets.encode('utf-8'))
			saves=int(saves.encode('utf-8'))
			catches=int(catches.encode('utf-8'))
			punched=int(punched.encode('utf-8'))
			passing_accuracy=float(passing_accuracy.encode('utf-8'))
			duels_won_per=float(duels_won_per.encode('utf-8'))
			aerial_duels_won_per=float(aerial_duels_won_per.encode('utf-8'))
			mins_per_goal=float(mins_per_goal.encode('utf-8'))
			saves_per_min=float(saves_per_min.encode('utf-8'))
			shots_saved_per=float(shots_saved_per.encode('utf-8'))
			
			writer.writerow([team_name.encode('utf-8'),player_name.encode('utf-8'),player_position.encode('utf-8'),player_games_played,player_minutes_played,player_stars,player_substitution_on,player_substitution_off,passes,passing_accuracy,duels_won,duels_lost,duels_won_per,aerial_duels_won,aerial_duels_lost,aerial_duels_won_per,recoveries,goals_conceded,mins_per_goal,clean_sheets,saves,saves_per_min,shots_saved_per,catches,punched])
		print "\n\n\n"
csvfile.close



"""

			#print "each details"
			#now extract player details
			#basic details are in td tag playercompare-table-2
			#comparision details are in div tag playercompare-container-2
			
			Aaron Ramsey
			Comparison
			MF Position -
			29 Games played 0
			2,009 Minutes Played 0
			23 Starts 0
			6 Substitution On 0
			9 Substitution Off 0


			General
			Defence & Discipline
			Attack
			General
			Aaron Ramsey Comparison
			1,641 Passes 0
			86% Passing Accuracy 0
			82.5% Passing Accuracy opp. Half (%) 0
			130 Duels Won 0
			161 Duels lost 0
			44.7% Duels won (%) 0
			18 Aerial duels won 0
			21 Aerial duels lost 0
			46.2% Aerial Duels Won (%) 0
			145 Recoveries 0


				Goalkeeping
				Defence & Discipline
				Goalkeeping
				Emiliano Martinez Comparison
				4 Goals Conceded 0
				75.3 Mins per goal conceded 0
				2 Clean Sheets 0
				4 Saves 0
				1.2 Saves per 90 minutes 0
				50% Shots Saved 0
				9 Catches 0
				1 Punches 0
"""